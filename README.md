Damini's Text Editor

Name: Damini Harshad Sheth
MIS ID: 111608016

This is a minimalistic text editor made by using basic structures and pointers.
It has basic operations like movements of the cursor(up, down, left, right), page up, cut, copy, paste, find, find & replace, save, save as and quit. 
If the file is bigger than the window, the page can be scrolled up or down as required.