#include"line.h"

#define PAGE_SIZE 500 
#define WIN_SIZE (LINES - 2)
#define LINE_SIZE 128

void init_line(LINE *s){
	s->line = (char*)malloc(LINE_SIZE * sizeof(char));
	s->size = LINE_SIZE;									//initialises a line
	s->line[0] = '\0';
}

void insert_char(LINE *s, char c, int index){
	int i, len;
	len = strlen(s->line);
	for (i = len ; i>= index; i--){
			s->line[i + 1] = s->line[i];                    //shifts the chars to right
	}
	s->line[index] = c;
}

void add_char(LINE *s, char c){
    insert_char(s, c, strlen(s->line));						//appends a char
}

void remove_char(LINE *s, int index){
	int i;
	int len = strlen(s->line);
	for (i = index; i < len; i++){
		s->line[i] = s->line[i + 1];                    //shifts the chars to left
	}
}