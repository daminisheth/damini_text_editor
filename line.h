#ifndef LINE_H
#define LINE_H

#include<stdio.h>
#include<sys/types.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>
#include<errno.h>
#include <ctype.h>
#include <string.h> 
#include <ncurses.h>

typedef struct LINE{
	char *line;
	int size;
} LINE;

void init_line(LINE *s);
void add_char(LINE *s, char c);
void insert_char(LINE *s, char c, int index);
void remove_char(LINE *s, int index);

#endif