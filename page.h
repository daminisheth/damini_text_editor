#ifndef PAGE_H
#define PAGE_H

#include<stdio.h>
#include<sys/types.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>
#include<errno.h>
#include <ctype.h>
#include <string.h> 
#include <ncurses.h>
#include "line.h" 

typedef struct PAGE{
    char filename[200];
	LINE *text;
	int numlines;
	int size;
} PAGE;

int count_lines(FILE *fp);
void save_file(PAGE *p);
int file_exists(PAGE *p, char *filename);
void open_file(PAGE *p, char* filename);
void init_page(PAGE *p, char *filename, int size);
void destroy_page(PAGE *p);
void insert_line(PAGE *p, int index);
void remove_line(PAGE *p, int index);
void print_page(const PAGE *p, int start, int end);
void expand_page(PAGE *p);
char* copy_line(PAGE *p, int y);
void paste_line(PAGE *p, char *temp, int y);
char* cut_line(PAGE *p, int y);
int find(PAGE *p);
void replace(PAGE *p, int index);

#endif