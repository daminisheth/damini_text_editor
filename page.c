#define PAGE_SIZE 500 
#define WIN_SIZE (LINES - 3)
#define LINE_SIZE 128

#include"page.h"
#include"main.h"
#include"line.h"

int count_lines(FILE *fp){
	int count = 0;
	char ch;
	while((ch = fgetc(fp)) != EOF)			// count number of lines of the page
		if( ch == '\n' )
			count++;
    fseek(fp, 0, SEEK_SET);
	return count;
}


void open_file(PAGE *p, char *filename){
	FILE *fp;
	fp = fopen(filename, "r");
	char ch;
	int i = 0, size;
	size = count_lines(fp) * 2;
    if(size < PAGE_SIZE)
        size = PAGE_SIZE;
	init_page(p, filename, size);
    if(fp == NULL){
        p->numlines = 1;
        return;
    }
	while((ch = fgetc(fp)) != EOF ){
		LINE *currline = &(p->text[i]);					//open existing file
    	if ( ch == '\n'){
    		i++;
    		p->numlines++;
		}
		else
			add_char(currline, ch);
	}
	fclose(fp);

} 

void save_file(PAGE *p){
	int i = 0, j;
	FILE *fp;
	fp = fopen(p->filename, "w");
	while(i < p->numlines ){
		j = 0;
		while( p->text[i].line[j] != '\0'){
			fputc(p->text[i].line[j], fp);
			j++;
		}
		fputc('\n', fp);
		i++;
	}
	fclose(fp);
}

/* IF CMDL: 
*check if file already exists, then open it
*else create it with that name
*/

int file_exists(PAGE *p, char *filename){
	int fp;
    fp = open(filename, O_RDONLY);
    if(fp != -1) {
        close(fp);
        return 1;
    }
    else
    	return 0;
}

//ELSE: create file called untitled.txt 

void create_newfile(){
	int fp;
    fp = open("UNTITLED.TXT", O_RDONLY | O_CREAT);
    if(fp != -1)
    	close(fp);
}

char* copy_line(PAGE *p, int y){
	char *temp = (char *)malloc(p->size * sizeof(char));
	strcpy(temp, p->text[y].line);							//copies current line
	return temp;
}

char* cut_line(PAGE *p, int y){
	char *temp = (char *)malloc(p->size * sizeof(char));
	strcpy(temp, p->text[y].line);							//cuts current line
	remove_line(p, y);
	move(y,1);
	return temp;
}

void paste_line(PAGE *p, char *temp, int y){
	insert_line(p, y);
	strcpy(p->text[y].line , temp);							//pastes current line
}

void init_page(PAGE *p, char *filename, int size){
	int i;
	p->numlines = 0;
	p->size = size;
	p->text = (LINE *)malloc(size * sizeof(LINE));
	for(i = 0; i < size; i++)
		init_line(&p->text[i]);                       //initializes all lines
    strcpy(p->filename, filename);	
}

void destroy_page(PAGE *p){
	int i;
	for(i = 0; i < p->numlines; i++)					//deletes whole page
		free(p->text[i].line);
	free(p->text);
} 

void insert_line(PAGE *p, int index){	
	int i;
	if( p->numlines >= p->size ) 
		expand_page(p);
	LINE newline;
	init_line(&newline);
	newline.line[0] = '\0';
	for(i = p->numlines - 1; i >= index; i--)
		p->text[i + 1] = p->text[i];				//same logic as insert char
	p->text[index] = newline;
	(p->numlines)++;
}

void remove_line(PAGE *p, int index){
	if(p->numlines > 1){
		free(p->text[index].line);					//removes the lines
		int i;
		for(i = index; i < p->numlines - 1; i++){	
			p->text[i] = p->text[i + 1];			//shifts all lines up
		}
		(p->numlines)--;
	}
}

void expand_page(PAGE *p){
	int newsize = p->size * 2;
	LINE *newline = malloc(newsize * sizeof(LINE));
	int i;
	for(i = 0; i < p->size; i++)					// copy old lines
		newline[i] = p->text[i];
	for(i = p->size; i < newsize; i++)				// init new lines
		init_line(newline + i);
	p->text = newline;
	p->size = newsize;
} 


void print_page(const PAGE *p, int start, int end){
	int i, line = 0;
	for(i = start; i < p->numlines && i < end; i++, line++){
		move(line, 0);
		clrtoeol();
		printw(" %s", p->text[i].line);
	}
    if(start < end){
        move(line, 0);   
        clrtoeol();					//if we deleted a line
        move(line-1, 1);
    }
	refresh();
}

int find(PAGE *p){
	int yMax, xMax, i = 0, indx;
	getmaxyx(stdscr, yMax, xMax);
	message("Enter the String to be searched for: ", yMax - 1);
	char *temp, *a;
	temp = ((char*)malloc(p->text->size));
	move(yMax - 1, 38);
	attron(A_REVERSE);
	echo();
    getstr(temp);									//get the word to be found
    noecho();
    attroff(A_REVERSE);
    clrtoeol();
	do{
		a = strstr(p->text[i].line, temp);			//comparing word with the buff file
		i++;
	}
	while (a == NULL && i < p->numlines);
	if (a){
		indx = a - (p->text[i-1].line) + 1;			//getting location
		print_page(p, 0, WIN_SIZE );
		move(i-1, indx);
		return indx;
	}
	else{
		message("Not Found", yMax - 1);
		return -1;
	}
}

void replace(PAGE *p, int index){
	int oldx, oldy, yMax, xMax, i = 0, j = 0, indx;
	getyx(stdscr, oldy, oldx);
	getmaxyx(stdscr, yMax, xMax);
	message("Enter the String to be searched for: ", yMax - 1);
	char *temp, *a, *ptr;
	temp = ((char*)malloc(p->text->size));
	move(yMax - 1, 38);
	attron(A_REVERSE);
	echo();
    getstr(temp);									//get the word to be found
    noecho();
    attroff(A_REVERSE);
    clrtoeol();
    int length = strlen(temp);
	char *c;
	c = (char*)malloc(p->text->size);
	message("Enter the NewString: ", yMax - 1);
	move(yMax - 1, 38);
	attron(A_REVERSE);
	echo();
	getstr(c);
	noecho();
	attroff(A_REVERSE);
	clrtoeol();
	int length2 = strlen(c);
	while (i < p->numlines){
   		ptr = p->text[i].line;
		do{
			a = strstr(ptr, temp);							//comparing word with the buff file
			if (a){
				indx = a - (p->text[i].line) + 1;			//getting location
				for (j = 0; j < length ; j++)
					remove_char(&p->text[i], indx - 1);
				for (j = 0; j < length2 ; j++){
						insert_char(&p->text[i], c[j], indx - 1);
						indx++;
				}	
				ptr = a + length;							//inserting the c in place of the word
				print_page(p, 0, WIN_SIZE );
			}
			else{
				print_page(p, 0, WIN_SIZE );
			}
		}
		while (a != NULL);
		i++;
	}
}