#include<stdio.h>
#include<sys/types.h>
#include<stdlib.h>
#include<fcntl.h>
#include<sys/stat.h>
#include<unistd.h>
#include<errno.h>
#include"main.h"
#include"page.h"
#include"line.h"

#define PAGE_SIZE 500 
#define WIN_SIZE (LINES - 3)
#define LINE_SIZE 128

int y_offset = 0;

int main(int argc, char *argv[]){	
	PAGE page;
	if(argc > 1){
        if(file_exists(&page, argv[1]))
		    open_file(&page, argv[1]);
        else{
            init_page(&page, argv[1], PAGE_SIZE);
            page.numlines = 1;
        }
	}
	else{
		init_page(&page, "Untitled.txt", PAGE_SIZE);
		page.numlines = 1;
	}
	initscr();
	noecho();
	keypad(stdscr, true);
	int beg = 0, y, x, i, index;
	int end = WIN_SIZE;
	char *temp, *tmp;
	print_page(&page, beg, end);
	getyx(stdscr, y, x);
	int oldy, oldx, yMax, xMax; 
	getmaxyx(stdscr, yMax, xMax);
	message("^W:QUIT | ^N:SAVE | ^L:SAVE AS | ^H:HELP | PgUp:PAGE UP |", yMax - 2);
	message("^O:COPY | ^X:CUT  | ^V:PASTE   | ^F:FIND |  ^R:REPLACE  |", yMax - 1);
	while(true){
        location(x, y);
		beg = y_offset;
		end = WIN_SIZE + y_offset;
		int ch = getch();
		switch(ch){
			case '\n': // enter - newline
				enter(&page, &y, &x);
				message("^O:COPY | ^X:CUT  | ^V:PASTE   | ^F:FIND |  ^R:REPLACE  |", yMax - 1);
				break;
			case CTRLO: // to copy the current line
				getyx(stdscr, y, x);
				temp = copy_line(&page, y + y_offset);
				break;
			case CTRLX: // to cut the line
				getyx(stdscr, y, x);
				temp = cut_line(&page,  y + y_offset);
				move( y + y_offset, x);
				print_page(&page, beg, end);
				break;
			case CTRLV: // to paste the copied/cut line on the current line
				getyx(stdscr, y, x);
				paste_line(&page, temp,  y + y_offset);
				move( y + y_offset, x);
				print_page(&page, beg, end);
				break;
			case CTRLW: // to quit
		        endwin();
				destroy_page(&page);
				return EXIT_SUCCESS;
			case CTRLN: // to save file
				save_file(&page);
				if(page.filename){
					move(yMax - 1, 0);
					clrtoeol();
		            printw("Saved as %s", page.filename);
                }
                else
                	message("Saved as Untitled.txt", yMax - 1);
				break;
			case CTRLL: //to save with a specified name
				message("Save As: ", yMax - 1);
				i = 0;
				getyx(stdscr, oldy, oldx);
				getmaxyx(stdscr, yMax, xMax);
				tmp = ((char*)malloc(LINE_SIZE));
				move(yMax - 1, 12);
				attron(A_REVERSE);
				echo();
				getstr(tmp);									//get the word to be found
				noecho();
				attroff(A_REVERSE);
				clrtoeol();
				strcpy(page.filename, tmp);
                save_file(&page);
                printw("Saved as %s", page.filename);
				print_page(&page, beg, end);
				break;
			 case '\t':
                for(i = 0; i < 4; i++){
                    insert_char(&page.text[y + y_offset], ' ', x - 1);
                    print_page(&page, beg, end);
                    move_right(&page, &x, &y);
                }
                break;
			case KEY_UP:
				move_up(&page, &x, &y);
				break;
			case KEY_DOWN:
				move_down(&page, &x, &y);
				break;
			case KEY_LEFT:
				move_left(&page, &x, &y);
				break;
			case KEY_RIGHT:
				message("^W:QUIT | ^N:SAVE | ^L:SAVE AS | ^H:HELP | PgUp:PAGE UP |", yMax - 2);
				move_right(&page, &x, &y);
				break;
			case PGUP:
				page_up(&page, &y, &x);
				break;
			case 127:
			case KEY_BACKSPACE:
				back_space(&page, &x, &y);
				break;
			case CTRLF:
				index = find(&page);		
				break;
			case CTRLR:
				replace(&page, index);
				print_page(&page, beg, end);
				message("^O:COPY | ^X:CUT  | ^V:PASTE   | ^F:FIND |  ^R:REPLACE  |", yMax - 1);
				break;
			case CTRLH:
				help(&page);
				break;
			default: // all characters
				if( isprint(ch) ){
					insert_char(&page.text[y + y_offset], ch, x - 1);
					print_page(&page, beg, end);
					move_right(&page, &x, &y);
				}
		message("^W:QUIT | ^N:SAVE | ^L:SAVE AS | ^H:HELP | PgUp:PAGE UP |", yMax - 2);
		message("^O:COPY | ^X:CUT  | ^V:PASTE   | ^F:FIND |  ^R:REPLACE  |", yMax - 1);
		}
	}
}

void back_space(PAGE *p, int *x, int *y){
	int i, a, b, c;
	char *temp;
	a = strlen(p->text[*y + y_offset].line);
	temp = ((char*)malloc(p->text->size));
	temp = copy_line(p, *y + y_offset);
	if (*y == 0 && *x == 1){
		print_page(p, y_offset, WIN_SIZE + y_offset);		//for first line and first char
		move(0, 1);
	}
	else if( *x > 1 && a > 0){
		remove_char(&p->text[*y + y_offset], *x - 2);
		print_page(p, y_offset, WIN_SIZE + y_offset);		// for lines with more than one char 
		move_left(p, x, y);				  
	}
	else if(a == 0 && *y != 0){
		b = strlen(p->text[*y + y_offset - 1].line) + 2;	//for empty lines	
		remove_line(p, *y + y_offset);
		*y++;
		*x = 1;
		clrtoeol();
		print_page(p, y_offset, WIN_SIZE + y_offset);
	}
	else if (a > 0 && *x == 1 && *y != 0 ){
		b = strlen(p->text[*y + y_offset - 1].line);		// for lines with only one char
		c = a + b + 1;
		for(i = 0; i < a ; i++){
			add_char(&p->text[*y + y_offset - 1], temp[i]);
		}
		*x = c;
		remove_line(p, *y + y_offset);
		move_up(p, x + 1, y);
		clrtoeol();
		print_page(p, y_offset, WIN_SIZE + y_offset);	
	}
	move(*y, *x);		
}

void enter(PAGE *p, int *y, int *x){
	int j, a;
	char *temp;
	a = strlen(p->text[*y + y_offset].line);
	temp = ((char*)malloc(p->text->size));
	if ( *x < a){
		temp = copy_line(p, *y + y_offset);
		paste_line(p, temp, *y + y_offset);						//in between a line
		for (j = *x - 1; j < a ; j++){
			remove_char(&p->text[*y + y_offset], *x - 1);
		}
		for (j = 1; j < *x; j++){
			remove_char(&p->text[*y + y_offset + 1], 0);
		}
		print_page(p, y_offset, WIN_SIZE + y_offset);	
		move_down(p, x, y);
	}
	else{
		insert_line(p, *y + y_offset + 1);						//end of a line
		print_page(p, y_offset, WIN_SIZE + y_offset);	
		move_down(p, x, y);
	}
}

void help(PAGE *p){
	int yMax, xMax;
	getmaxyx(stdscr, yMax, xMax);
	move(0,0);
	clrtobot();
	printw("HELP OPTIONS:\nControl + W : To quit the editor\nControl + N : To Save the cuurent file(will be saved as Untitled.txt if not saved earlier with a name)\nControl + L : To Save the file with a specific name\nControl + O : To copy the current line\nControl + X : To Cut the current line\nControl + V : To Paste the copied/cut line\nControl + F : To find and go the first occurence of the entered word\nControl + R : To find a word and replace it with another word (all occurences)\nPgUp Key:Moves the page(screen) up - for long files\n\n Press any key to Exit");
	char ch = getch();
	if (ch){
		move(0,0);
		clrtobot();
	   	print_page(p, y_offset, WIN_SIZE + y_offset);
	   	message("^W:QUIT | ^N:SAVE | ^L:SAVE AS | ^H:HELP | PgUp:PAGE UP |", yMax - 2);
		message("^O:COPY | ^X:CUT  | ^V:PASTE   | ^F:FIND |  ^R:REPLACE  |", yMax - 1);
	}
}

void location(int x, int y){
	int oldx, oldy, yMax, xMax;						// to get instantaneous position of cursor
	getyx(stdscr, oldy, oldx);						// y starts from 0 and x from 1
	getmaxyx(stdscr, yMax, xMax);		
	mvprintw(yMax - 1, xMax - 30, "x:%d y:%d offset:%d   ", x, y, y_offset);
	move(oldy, oldx);
}

void message(char *message, int yind){
	int oldy, oldx, yMax, xMax; 
	getyx(stdscr, oldy, oldx);						// display message at the bottom
	attron(A_REVERSE);								// for status bar
	move(yind, 0);
	clrtoeol();
	printw(message);
	attroff(A_REVERSE);
	move(oldy, oldx);
}

/* cursor movements*/
void move_left(PAGE *p, int *x, int *y){
	int yMax, xMax, i;
	getmaxyx(stdscr, yMax, xMax);
	if (*x > 1){
		--(*x);
	}
	else{
		if (* y > 0 && y_offset == 0){
			--(*y);
			for(i = 0; i < strlen(p->text[*y + y_offset].line); i++){
				if ((*x))
					(*x)++; 
			}
		}
		else if(y_offset > 0){
			--(y_offset);
			for(i = 0; i < strlen(p->text[*y + y_offset].line); i++){
				if ((*x))
					(*x)++; 
			}
		}
	}
	print_page(p, 0 + y_offset, WIN_SIZE + y_offset);
	move(*y, *x);
}

void move_right(PAGE *p, int *x, int *y){
	int yMax, xMax, a;
	getmaxyx(stdscr, yMax, xMax);
	a = strlen(p->text[*y + y_offset].line);
	if ((*x) - 1 < a){
		++(*x);
	}
	else if (*y == yMax - 4){
		int *q = x;
		int b = strlen(p->text[(*y + y_offset) + 1].line);
		if (*y == yMax - 4 && *x == a + 1 && *y + y_offset < p->numlines){
			if (b){
				++(y_offset);
				*x = 1;
			}
		}
		else if (*q == 1 || *y + y_offset  < p->numlines - 2){
			++(y_offset);
			*x = 1;		
		}
		move(y_offset, *x);		
	}
	
	else {
		int *q = x;
		int b = strlen(p->text[(*y + y_offset) + 1].line);
		if (b){
			++(*y);
			*x = 1;
		}
		else if (*y < p->numlines - 1  && y_offset == 0){
			(*y)++;
			*x = 1;	
		}
		else if (*q == 1 && *y < p->numlines - 1  && y_offset == 0){
			++(*y);
			*x = 1;		
		}	
	}
	print_page(p, y_offset,  WIN_SIZE + y_offset);
	move(*y, *x);
}

void move_up(PAGE *p, int *x, int *y){
	int b;
	if (*y == 0){
		*y = 0;
	print_page(p, y_offset, WIN_SIZE + y_offset);	
	}
	else if( *y > 0 && y_offset == 0 ){
		b = strlen(p->text[((*y) - 1) + y_offset].line);
		if (*x > b + 1){
			*x = b + 1;
			--(*y);
		}
		else
			--(*y);
		print_page(p, y_offset, WIN_SIZE + y_offset);	
	}
	else if (y_offset > 0){
		int off;
		off = y_offset;
		b = strlen(p->text[((*y) - 1) + y_offset].line);
		if (*x > b + 1){
			*x = b + 1;
			--(y_offset);
		}
		else
			--(y_offset);
		print_page(p, y_offset, *y + off);	
	}
	move(*y, *x);
}

void move_down(PAGE *p, int *x, int *y){
	int b;
	if ((*y)+1){
		b = strlen(p->text[((*y) + 1) + y_offset].line);
		if( *y < WIN_SIZE - 1 && *y < p->numlines  - 1 ){
			*x = b + 1;
			++(*y);
		}
		else if ( *y + y_offset < p->numlines - 1 ){
			++(y_offset);
			*x = b + 1;
		}
	}
	else{
		if (y_offset > p->numlines - 2){
			*x = 1;
			++(y_offset);		
		}
		else{
			*x = 1;
			++(*y);
		}
	}
	print_page(p, y_offset, WIN_SIZE + y_offset);	
	move(*y, *x);
}

void page_up(PAGE *p, int *y, int *x){
	int yMax, xMax;
	getmaxyx(stdscr, yMax, xMax);
	if (*y < yMax - 4 && y_offset == 0){
		*y = 0;
	}
	else if (y_offset > 0 && y_offset < yMax - 1){
		*y = y_offset;
		y_offset = 0;
	}
	else{
		y_offset = y_offset - *y;
	}
	*x = 1;
	print_page(p, y_offset, WIN_SIZE + y_offset);	
	move(*y, *x);
}