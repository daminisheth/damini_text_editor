#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h> 
#include <ncurses.h>
#include"page.h"

#define DEL 330
#define END 360
#define PGDOWN 338
#define PGUP 339
#define CTRLH 8
#define CTRLV 22
#define CTRLX 24
#define CTRLO 15
#define CTRLL 12
#define CTRLF 6
#define CTRLN 14
#define CTRLR 18
#define CTRLW 23

void message(char *message, int y);
void location(int x, int y);
void help(PAGE *p);

void enter(PAGE *p, int *x, int *y);
void back_space(PAGE *p, int *x, int *y);
void move_left(PAGE *p, int *x, int *y);
void move_right(PAGE *p, int *x, int *y);
void move_up(PAGE *p, int *x, int *y);
void move_down(PAGE *p, int *x, int *y);
void page_up(PAGE *p, int *y, int *x);
