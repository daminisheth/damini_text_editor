project: main.o page.o line.o
	cc main.o page.o line.o -o project -lncurses
main.o: main.c page.h line.h
	cc main.c -c -lncurses
page.o: page.c main.h line.h
	cc page.c -c -lncurses
line.o: line.c
	cc line.c -c -lncurses
